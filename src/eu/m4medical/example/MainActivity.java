package eu.m4medical.example;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	private static final int ACTIVITY_MTRACE=0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		EditText edtx = (EditText) findViewById(R.id.add_filepath);
		File f=Environment.getExternalStorageDirectory();		
		edtx.setText(f.getAbsolutePath()+"/Android/data/eu.m4medical.example/files/");
		
		Button btn = (Button) findViewById(R.id.start);
		btn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				
				startMTracePC();
				
			}
			
		});
	}
	
	private void startMTracePC() {
		EditText stx,ntx,itx,atx,btx,htx,wtx,sxtx,fntx,fptx,fttx;
		stx = (EditText) findViewById(R.id.add_surname);
		ntx = (EditText) findViewById(R.id.add_name);
		itx = (EditText) findViewById(R.id.add_id);
		atx = (EditText) findViewById(R.id.add_age);
		btx = (EditText) findViewById(R.id.add_birth);
		htx = (EditText) findViewById(R.id.add_height);
		wtx = (EditText) findViewById(R.id.add_weight);
		sxtx = (EditText) findViewById(R.id.add_sex);
		fntx = (EditText) findViewById(R.id.add_filename);
		fptx = (EditText) findViewById(R.id.add_filepath);
		fttx = (EditText) findViewById(R.id.add_filetype);

		
		
		Intent i = new Intent("eu.m4medical.mtracepc.ECG");

		i.putExtra("surname", stx.getText().toString());
		i.putExtra("name", ntx.getText().toString());
		i.putExtra("ID", itx.getText().toString());
		i.putExtra("age", (short)Integer.parseInt(atx.getText().toString()));
		i.putExtra("birth", btx.getText().toString());
		i.putExtra("height", (short)Integer.parseInt(htx.getText().toString()));
		i.putExtra("weight", (short)Integer.parseInt(wtx.getText().toString()));		
		i.putExtra("sex", (byte)Integer.parseInt(sxtx.getText().toString()));		
		i.putExtra("filename", fntx.getText().toString());
		i.putExtra("filepath", fptx.getText().toString());
		i.putExtra("filetype", (byte)Integer.parseInt(fttx.getText().toString()));

		startActivityForResult(i,ACTIVITY_MTRACE);
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);        
        
        switch(requestCode) {      
                              
            case ACTIVITY_MTRACE:
            	if(resultCode == Activity.RESULT_CANCELED) {
            		Toast toast = Toast.makeText(this.getApplicationContext(), 
            				"Error code: "+intent.getByteExtra("error",(byte) -1), 5);
            		toast.show();            	
            	}
            	else {
            		Toast toast = Toast.makeText(this.getApplicationContext(), 
            				"Examination saved successfully", 5);
            		toast.show(); 
            	}
            	break;
                
        }
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
